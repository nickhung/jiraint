#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
Copyright (C) 2013-2016  Diego Torres Milano
Created on 2016-09-15 by Culebra v11.5.12
                      __    __    __    __
                     /  \  /  \  /  \  /  \ 
____________________/  __\/  __\/  __\/  __\_____________________________
___________________/  /__/  /__/  /__/  /________________________________
                   | / \   / \   / \   / \   \___
                   |/   \_/   \_/   \_/   \    o \ 
                                           \_____/--<
@author: Diego Torres Milano
@author: Jennifer E. Swofford (ascii art snake)
'''


import re
import sys
import os

from com.dtmilano.android.viewclient import ViewClient

TAG = 'CULEBRA'

_s = 5
_v = '--verbose' in sys.argv
project_root = os.path.dirname(os.path.abspath(__file__))
screen_shot_folder = 'screenshot'
screen_shot_path = os.path.join(project_root, screen_shot_folder)
if not os.path.exists(screen_shot_path):
    os.makedirs(screen_shot_path)


kwargs1 = {'ignoreversioncheck': False, 'verbose': False, 'ignoresecuredevice': False}
device, serialno = ViewClient.connectToDeviceOrExit(**kwargs1)
kwargs2 = {'forceviewserveruse': False, 'useuiautomatorhelper': False, 'ignoreuiautomatorkilled': True, 'autodump': False, 'startviewserver': True, 'compresseddump': True}
vc = ViewClient(device, serialno, **kwargs2)

filename = screen_shot_path + '/${serialno}-${focusedwindowname}-${timestamp}.png'

vc.dump(window=-1)
vc.writeImageToFile(filename, 'PNG', None, False, False)
vc.findViewByIdOrRaise("mobisocial.arcade:id/timestamp").touch()
vc.sleep(_s)
vc.dump(window=-1)
vc.writeImageToFile(filename, 'PNG', None, False, False)
vc.findViewWithContentDescriptionOrRaise(u'''Navigate up''').touch()
vc.sleep(_s)
vc.dump(window=-1)
vc.findViewWithTextOrRaise("Communities").touch()
vc.sleep(_s)
vc.dump(window=-1)
vc.writeImageToFile(filename, 'PNG', None, False, False)
vc.findViewByIdOrRaise("mobisocial.arcade:id/oma_sub").touch()
vc.sleep(_s)
vc.dump(window=-1)
vc.writeImageToFile(filename, 'PNG', None, False, False)
vc.findViewWithContentDescriptionOrRaise(u'''Navigate up''').touch()
vc.sleep(_s)
vc.dump(window=-1)
vc.findViewByIdOrRaise("mobisocial.arcade:id/oma_community_post_count").touch()
vc.sleep(_s)
vc.dump(window=-1)
vc.writeImageToFile(filename, 'PNG', None, False, False)
vc.findViewWithContentDescriptionOrRaise(u'''Navigate up''').touch()
vc.sleep(_s)
vc.dump(window=-1)
vc.findViewWithTextOrRaise("Profile").touch()
vc.sleep(_s)
vc.dump(window=-1)
vc.writeImageToFile(filename, 'PNG', None, False, False)
